# hello.rb 

A demo commandline application that can be used to time HTTP/HTTPS get (HEAD only)
actions against a given url, specified on command line.

sample console session:

```


Warrens-MacBook-Pro:hai wpostma$ ./hello.rb https://www.gitlab.com
2016-10-10 16:15:27 -0400 : 35.27  ms 
2016-10-10 16:16:01 -0400 : 37.17  ms 
2016-10-10 16:16:35 -0400 : 35.12  ms 
2016-10-10 16:17:09 -0400 : 31.66  ms 
2016-10-10 16:17:43 -0400 : 38.64  ms 
2016-10-10 16:18:17 -0400 : 34.81  ms 
2016-10-10 16:18:51 -0400 : 37.23  ms 
2016-10-10 16:19:24 -0400 : 34.57  ms 
2016-10-10 16:19:58 -0400 : 34.99  ms 
2016-10-10 16:19:58 -0400 : 34.05  ms 

Warrens-MacBook-Pro:hai wpostma$ ./hello.rb
hello : periodic url fetcher and response time logger v.01
hello <url> --request <request-count=10> --time <time-limit=300s>  
examples:
   hello http://www.gitlab.com/ -r 10 -t 300 
   will run http get against provided url every 30 seconds for 300 seconds (5 minutes)
   and output http response times


```


