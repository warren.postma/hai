#!/usr/bin/env ruby


class Hello 
	require 'optparse'
	require 'net/http'
	require 'net/https'
	require 'benchmark'
	def initialize
		@start_time = nil
		@cmd = "hello"
		@url = ""
		@repeat_count = 10
		@time_limit = 300
	end

	def title
		puts "#{@cmd} : periodic url fetcher and response time logger v.01"
	end

	def help
		title
		puts "#{@cmd} <url> --request <request-count=10> --time <time-limit=300s>  "
		puts "examples:"
		puts "   hello http://www.gitlab.com/ -r 10 -t 300 "
		puts "   will run http get against provided url every 30 seconds for 300 seconds (5 minutes)"
		puts "   and output http response times"
	end

	def parse
		@opt_parser = OptionParser.new do |opt|  
			opt.banner = "Usage: #{@cmd}  URL [OPTIONS]"
			opt.separator  ""
			opt.separator  "Requires"
			opt.separator  "     URL: a valid http or https url"
			opt.separator  ""
			opt.separator  "Options"

			opt.on("-r","--request TIMES","request a certain number of times") do |value|
				@repeat_count = value.to_i
			end

			opt.on("-t","--time LIMIT","run duration in seconds (ie 300 = 5 min)") do |value|
				@time_limit  = value.to_i
			end

			opt.on("-h","--help","help") do
				help
			end
		end
		
		@opt_parser.parse!
		@url = ARGV[0]
		
	end
	
	def setup
		@uri = URI(@url)
		@use_ssl = @uri.scheme == 'https'
		@path = "/"
		@start_time = Time.now
		# TWO TIMES OVER 5 MINUTES = ONE DELAY OF 5 MINUTES
		@delay_time = (@time_limit+0.00)/(@repeat_count-1)
	end

	def get_head
		t1 = Time.now
		b1 = nil
		Net::HTTP.start(@uri.host, @uri.port,
  				:use_ssl => @use_ssl) do |http|
		
					  
				b1 = Benchmark.realtime {
 			      request = Net::HTTP::Get.new @uri
				  response_head= http.head @path
				} * 1000
				# puts ">",response_head.header.to_s # could be redirect (permanently moved )
		
				print Time.now, " : #{b1.round(2)}  ms \n"
	
		end
		
	end

	def main
		if ARGV.empty?
			help
		else
			parse
			if (@repeat_count<1)
				raise ArgumentError, "--request count value invalid"
			end
			if (@time_limit<1)
				raise ArgumentError, "--time limit value invalid"
			end
			# one time stuff, get URI, etc
			setup
			# puts @url, @repeat_count, @time_limit
			for i in 1..@repeat_count do
				get_head
				sleep(@delay_time) unless i == @repeat_count
			end
		end
	end

end

def run
	n = Hello.new
	n.main
end

run

